using System.Reflection;

[assembly: AssemblyTitle("FX.Configuration.Azure")]
[assembly: AssemblyDescription("FX Configuration Azure")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("FriendlyX")]
[assembly: AssemblyProduct("FX.Configuration.Azure")]
[assembly: AssemblyCopyright("FriendlyX Copyright © 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("0.4.0.0")]
[assembly: AssemblyFileVersion("0.4.0.17920")]
